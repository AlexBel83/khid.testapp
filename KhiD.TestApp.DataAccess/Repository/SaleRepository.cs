﻿using KhiD.TestApp.DataAccess.Interface;
using KhiD.TestApp.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhiD.TestApp.DataAccess.Repository
{
    public class SaleRepository : ISaleRepository
    {
        public List<FactInternetSale> GetSaleOrders(DateTime? dateBegin = null, DateTime? dateEnd = null)
        {
            using (AdventureWorkModelEntities entites = new AdventureWorkModelEntities())
            {
                var orders = entites.FactInternetSales.AsNoTracking()
                    .Where(x => (!dateBegin.HasValue || x.OrderDate >= dateBegin.Value)
                        && (!dateEnd.HasValue || x.OrderDate <= dateEnd.Value))
                    .Include(x => x.DimCustomer)
                    .ToList();
               
                return orders;
            }

        }

        public void UpdateCustomerOrder(string saleOrderNumber, byte saleOrderLineNumber, int customerId)
        {
            using (AdventureWorkModelEntities entites = new AdventureWorkModelEntities())
            {
                var order = entites.FactInternetSales.FirstOrDefault(x => x.SalesOrderNumber == saleOrderNumber && x.SalesOrderLineNumber == saleOrderLineNumber);

                if (order != null)
                {
                    order.CustomerKey = customerId;
                    entites.SaveChanges();
                }

            }
        }
    }
}
