﻿using KhiD.TestApp.Logic.Interface;
using KhiD.TestApp.Logic.Service;
using KhiD.TestApp.Modules.SaleOrder.Extensions;
using KhiD.TestApp.Modules.SaleOrder.Models;
using KhiD.TestApp.Modules.SaleOrder.ViewModels;
using System.Windows;
using System.Windows.Controls;

namespace KhiD.TestApp.Modules.SaleOrder.Views
{
    /// <summary>
    /// Interaction logic for SaleOrderView.xaml
    /// </summary>
    public partial class SaleOrderView : UserControl
    {
        private ISaleService _srvSale;

        public SaleOrderView(SaleOrderViewModel model)
        {
            InitializeComponent();

            _srvSale = new SaleService();

            DataContext = model;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((e.AddedItems.Count > 0) && (e.RemovedItems.Count > 0))
            {
                CustomerModel newCustomer = (CustomerModel)e.AddedItems[0];
                CustomerModel oldCustomer = (CustomerModel)e.RemovedItems[0];
                SaleOrderModel saleOrder = (SaleOrderModel)gridCustomer.SelectedItem;

                if (newCustomer.Id != oldCustomer.Id)
                {
                    saleOrder.Customer = newCustomer.Name;

                    _srvSale.UpdateCustomerOrder(saleOrder.SaleOrderNumber, saleOrder.SaleOrderLineNumber, newCustomer.Id);
                }                
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            var orders = _srvSale.GetSaleOrders(dpBegDate.SelectedDate, dpEndDate.SelectedDate).ToSaleOrderModel();

            SaleOrderViewModel model = (DataContext as SaleOrderViewModel);

            model.Orders.Clear();
            foreach (var order in orders)
                model.Orders.Add(order);
        }
    }
}