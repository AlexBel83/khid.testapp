﻿using KhiD.TestApp.DataAccess.Interface;
using KhiD.TestApp.DataAccess.Model;
using KhiD.TestApp.DataAccess.Repository;
using KhiD.TestApp.Logic.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhiD.TestApp.Logic.Service
{
    public class CustomerService : ICustomerService
    {
        private ICustomerRepository _repoCustomer;

        public CustomerService()
        {
            _repoCustomer = new CustomerRepository();
        }

        public List<DimCustomer> GetAllCustomers()
        {
            var customers = _repoCustomer.GetAllCustomers();

            customers = customers.OrderBy(x => x.FirstName).ThenBy(x => x.LastName).ToList();

            return customers;
        }
    }
}
