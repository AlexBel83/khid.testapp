﻿using KhiD.TestApp.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhiD.TestApp.DataAccess.Interface
{
    public interface ICustomerRepository
    {
        List<DimCustomer> GetAllCustomers();    
    }
}