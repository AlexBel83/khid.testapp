﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhiD.TestApp.Modules.SaleOrder.Models
{
    public class CustomerModel : BindableBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
