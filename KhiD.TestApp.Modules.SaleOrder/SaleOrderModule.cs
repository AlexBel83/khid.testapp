﻿using KhiD.TestApp.Modules.SaleOrder.Views;
using Prism.Modularity;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhiD.TestApp.Modules.SaleOrder
{
    public class SaleOrderModule : IModule
    {
        IRegionManager _regionManager;

        public SaleOrderModule(RegionManager regionManager)
        {
            _regionManager = regionManager;
        }

        public void Initialize()
        {
            _regionManager.RegisterViewWithRegion("ContentSaleOrder", typeof(SaleOrderView));
        }
    }
}
