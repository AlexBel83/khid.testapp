﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhiD.TestApp.Modules.SaleOrder.Models
{
    public class SaleOrderModel : BindableBase
    {
        public string SaleOrderNumber { get; set; }
        public byte SaleOrderLineNumber { get; set; }
        public DateTime? OrderDate { get; set; }
        public int CustomerId { get; set; }
        public string Customer { get; set; }
    }
}
