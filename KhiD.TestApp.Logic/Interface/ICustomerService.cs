﻿using KhiD.TestApp.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhiD.TestApp.Logic.Interface
{
    public interface ICustomerService
    {
        List<DimCustomer> GetAllCustomers();
    }
}
