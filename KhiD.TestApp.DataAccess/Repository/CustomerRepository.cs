﻿using KhiD.TestApp.DataAccess.Interface;
using KhiD.TestApp.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KhiD.TestApp.DataAccess.Repository
{
    public class CustomerRepository : ICustomerRepository
    {
        public List<DimCustomer> GetAllCustomers()
        {
            using (AdventureWorkModelEntities entites = new AdventureWorkModelEntities())
            {
                var customers = entites.DimCustomers.AsNoTracking().ToList();

                return customers;
            }
        }
    }
}
