﻿using KhiD.TestApp.DataAccess.Interface;
using KhiD.TestApp.DataAccess.Model;
using KhiD.TestApp.Logic.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KhiD.TestApp.DataAccess.Repository;

namespace KhiD.TestApp.Logic.Service
{
    public class SaleService : ISaleService
    {
        private ISaleRepository _repoSale;

        public SaleService()
        {
            _repoSale = new SaleRepository();
        }

        public IEnumerable<FactInternetSale> GetSaleOrders(DateTime? dateBegin = null, DateTime? dateEnd = null)
        {
            return _repoSale.GetSaleOrders(dateBegin, dateEnd);
        }

        public void UpdateCustomerOrder(string saleOrderNumber, byte saleOrderLineNumber, int customerId)
        {
            _repoSale.UpdateCustomerOrder(saleOrderNumber, saleOrderLineNumber, customerId);
        }
    }
}
