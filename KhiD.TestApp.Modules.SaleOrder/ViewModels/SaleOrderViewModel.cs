﻿using KhiD.TestApp.Logic.Service;
using KhiD.TestApp.Modules.SaleOrder.Extensions;
using KhiD.TestApp.Modules.SaleOrder.Models;
using Prism.Mvvm;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;

namespace KhiD.TestApp.Modules.SaleOrder.ViewModels
{
    public class SaleOrderViewModel : BindableBase
    {
        private ObservableCollection<SaleOrderModel> _orders = new ObservableCollection<SaleOrderModel>();
        private ObservableCollection<CustomerModel> _customers = new ObservableCollection<CustomerModel>();

        public SaleOrderViewModel()
        {
            LoadOrdersBackground();
            LoadCustomersBackground();
        }

        public ObservableCollection<SaleOrderModel> Orders
        {
            get
            {
                return this._orders;
            }

            set
            {
                SetProperty(ref _orders, value);
            }
        }

        public ObservableCollection<CustomerModel> Customers
        {
            get
            {
                return this._customers;
            }

            set
            {
                SetProperty(ref _customers, value);
            }
        }

        #region Load Orders

        private void LoadOrdersBackground()
        {
            Mouse.OverrideCursor = Cursors.Wait;
            
            BackgroundWorker worker = new BackgroundWorker();

            worker.WorkerReportsProgress = true;

            worker.DoWork += new DoWorkEventHandler(this.LoadOrdersBackground);
            worker.ProgressChanged += new ProgressChangedEventHandler(this.LoadOrdersBackgroundProgress);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.LoadOrdersBackgroundComplete);

            worker.RunWorkerAsync();
        }

        private void LoadOrdersBackground(object sender, DoWorkEventArgs e)
        {
            var srvSale = new SaleService();

            BackgroundWorker source = (BackgroundWorker)sender;

            var orders = srvSale.GetSaleOrders();
            int index = 0;

            foreach (var order in orders)
            {
                source.ReportProgress(++index, order.ToSaleOrderModel());
            }
        }

        private void LoadOrdersBackgroundProgress(object sender, ProgressChangedEventArgs e)
        {
            this.Orders.Add((SaleOrderModel)e.UserState);
        }

        private void LoadOrdersBackgroundComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            Mouse.OverrideCursor = null;
        }

        #endregion Load Orders

        #region Load Customers

        private void LoadCustomersBackground()
        {
            Mouse.OverrideCursor = Cursors.Wait;

            BackgroundWorker worker = new BackgroundWorker();

            worker.WorkerReportsProgress = true;

            worker.DoWork += new DoWorkEventHandler(this.LoadCustomersBackground);
            worker.ProgressChanged += new ProgressChangedEventHandler(this.LoadCustomersBackgroundProgress);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.LoadCustomersBackgroundComplete);

            worker.RunWorkerAsync();
        }

        private void LoadCustomersBackground(object sender, DoWorkEventArgs e)
        {
            var srvCustomer = new CustomerService();

            BackgroundWorker source = (BackgroundWorker)sender;

            var customers = srvCustomer.GetAllCustomers();
            int index = 0;

            foreach (var customer in customers)
            {
                source.ReportProgress(++index, customer.ToCustomerModel());
            }
        }

        private void LoadCustomersBackgroundProgress(object sender, ProgressChangedEventArgs e)
        {
            this.Customers.Add((CustomerModel)e.UserState);
        }

        private void LoadCustomersBackgroundComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            Mouse.OverrideCursor = null;
        }

        #endregion Load Customers
    }
}
