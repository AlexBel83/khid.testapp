﻿using KhiD.TestApp.DataAccess.Model;
using KhiD.TestApp.Modules.SaleOrder.Models;
using System.Collections.Generic;
using System.Linq;

namespace KhiD.TestApp.Modules.SaleOrder.Extensions
{
    public static class SaleOrderExtensions
    {
        public static CustomerModel ToCustomerModel(this DimCustomer value)
        {
            CustomerModel model = new CustomerModel()
            {
                Id = value.CustomerKey,
                Name = $"{value.Title} {value.FirstName} {value.LastName}"
            };

            return model;
        }

        public static IEnumerable<CustomerModel> ToCustomerModel(this IEnumerable<DimCustomer> values)
        {
            return values.Select(x => x.ToCustomerModel());
        }

        public static SaleOrderModel ToSaleOrderModel(this FactInternetSale value)
        {
            SaleOrderModel model = new SaleOrderModel()
            {
                CustomerId = value.CustomerKey,
                Customer = $"{value.DimCustomer.Title} {value.DimCustomer.FirstName} {value.DimCustomer.LastName}",
                OrderDate = value.OrderDate,
                SaleOrderNumber = value.SalesOrderNumber,
                SaleOrderLineNumber = value.SalesOrderLineNumber
            };

            return model;
        }

        public static IEnumerable<SaleOrderModel> ToSaleOrderModel(this IEnumerable<FactInternetSale> values)
        {
            return values.Select(x => x.ToSaleOrderModel());
        }
    }
}
